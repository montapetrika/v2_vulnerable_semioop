<!-- after adding JS code some serious lag started to happen between changing CSS and it applying - caching older versions of still developing page -->
<?php
// this (and all of includes, initiations etc.. could go in header file, where there are options on which text and buttons to show, depending un URL/URI.
//that's to do (to learn) 

/* weirder sku validation from php side option:
 * post submit sku uniqueness gets checked,
 * if not unique 
 * - does not upload the values
 * - saves posted values as 'temporary' variables
 * - places those variables as input placeholders (but how would it set which select option is checked? (some js?) and reloads the form with those..
 * [how whithout js??]
 * 
 */
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
// error_reporting(E_ALL);  
require 'includes/class-autoload.php'; //or include_once

//initiating and 'reading into' variables appropriate values from METHODS (because obviosly I don't understand __construct that well
 
 //separate class file - validation?  that's initiated beforehand here?
$skuObject = new SkuValues();
$uniqueSkuArray = $skuObject->getAllUniqueSku();
//print_r($uniqueSkuArray);
$jsonSku = $skuObject->jsonSku();   //for js to check; AJAX could be used for validation    //or can be passed as echo json_encode($uniqueSkuArray);




// usually form action cannot directly call a class and it's method (some frameworks can) [(object?) mapping required]; you need to call special script file [or the condition can be in this same file]
//can be separate script file that requires class file and instantiates the class and calls particular method (this, but in separate file; that file can contain several if isset etc... and appropriate action)
// ((or script file includes file in which there is  class file (or autoload) request/include + instatiating [like header file]) and that script file just asks for method when certain condition is met, no instantiating in the script file) 

//can be put in separate scirpt file which gets called from form action //or maybe at the top of the class file
/*
require_once 'classes/updating.php';        //we have autoload
*/
/* MASS DELETE */ /*YES, COULD BE SEPARATE SCRIPT FILE FOR FORM ACTION*/
//or if($POST) {  //not in separate file
 if ( ($_SERVER['REQUEST_METHOD'] == 'POST') && (isset($_REQUEST['delete_button'])) ) { //or the same with (isset($_POST['delete_button']))  //distinguisher if several ifs in separate script file
     if ($_POST['delete_checkbox']) {
        $deleteProduct = new Updating();
        $deleteProduct->deleteProduct();    
     }
     else header("Location: index.php");    //otherwise notice: undefined index; delete_checkbox
 }
 
 /* ADDING NEW PRODUCT*/ /*YES, COULD BE SEPARATE SCRIPT FILE FOR FORM ACTION*/         //VALIDATION should be SEPARATE CLASS
 
 //if($_POST) {    //putting POST['name of input fields or form'] -> says undefined index 
//don't have to worry about POST index names if there is only one post in the page where this if lives;
// or if separate script file carries out one action call; there could be one script file for several form actions, but then distinction in that file needs to be made;
//if($_SERVER['REQUEST_METHOD'] == 'POST') {    //or if($POST) {  //not in separate file
 
if ( ($_SERVER['REQUEST_METHOD'] == 'POST') && (isset($_REQUEST['save_button'])) ) {  //distinguisher if several ifs in separate script file
    $skuValidation = new SkuValues();
    $uniqueSkuArray = $skuValidation->getAllUniqueSku();    
    if (!in_array(strtoupper($_POST['sku']), $uniqueSkuArray)) {        //CASE SENSITIVE comparison; otherwise input could be sent using different case usage, causing DB side error (UNIQUE still catches)
        $addProduct = new Updating();
        $addProduct->addProduct();
        header("Location: index.php");
    }
    else $msg = "* This SKU (".strtoupper($_POST['sku']).") already exists! No entry added!";
  }   //if form action="index.php", no need for this; only if action ""/self






?>