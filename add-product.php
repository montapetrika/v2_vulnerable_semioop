<?php
require 'scripts.php';
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
	<meta name='author' content='Monta Petrika' />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Junior Developer Test Task for Scandiweb</title>
        <link rel="stylesheet" type="text/css" href="style.css" />   
<!-- script *hide all fieldsets (so previous would disappear when switching options several times) [for loop] or [Array.prototype.map() with variable for array], 
*show chosen option's fieldset;  -->  
<!-- WHAT DOES THAT MEAN??? ^ I don't remember anymore -->
    </head>
<!--
javascript (iteration) stops working when this line is introduced (on it's own or as else)
document.getElementById(option.value).style.display = 'none';
FIXED!! doesn't work without if(option.value) [first value is "", so all of elements with empty IDs ar called?? => stops working / or just errors out]
took me literally half a day to get this code to work, until the lightbulb moment, thinking through what each option does
-->    
  
    <script>
        function showProductParameters(id) {                
            var selectedProductType = document.getElementById(id).value;      //fieldset's id === values of the options        
            for (var option of document.getElementById(id).options){
                if (option.value) {    //!!!!! first value is "", getElementById with 'falsy value' does not work; script stops working at that point without this line
                    if (option.value == selectedProductType) {
                        document.getElementById(selectedProductType).style.display = 'block';
                        document.getElementById(selectedProductType).disabled = false;    
                                          //           document.getElementById(selectedProductType).input.required = true;      
                                          //V2: how to make all input fields required at once? for each?
                        //disabling fieldset automatically DISABLES BEING REQUIRED, so html can contain required for all inputs (without disabling can't be posted without them all being filled and sent

                        //BUT!!! INFORMATION IN INPUT FIELD DOES NOT GET ERASED WHEN SWITCHING BETWEEN TYPES (atleast does not get posted)
                        //^how to do that???

                     }
                     else {document.getElementById(option.value).style.display = 'none';
                        // document.getElementById(option.value).input.value = ''; //shows only DVD, rest can not be selected
                           document.getElementById(option.value).disabled = true;   //disable fieldset from sending data
                                       //    document.getElementById(option.value).input.required = false;    //this version does not work                          
                     }
                // if-else can be written as one line:    
                //     document.getElementById(option.value).style.display = option.value == selectedProductType? 'block' : 'none';
                }
            }     // AND ADD / DELETE ATRRIBUTE REQUIRED, so there is no need for additional validation lines, 
                    // because if html fields are all required, can't POST while all types haven't been swithed and filled..
        }
        
        var skuArray = <?php echo $jsonSku ?>;      //strtoupper in skuvalues.php  //could be here also for extra safety though
        function validateSku() {
            var inputText = document.getElementById("sku").value.toUpperCase();     //comparison is CASE SENSITIVE!!!
        //    alert(skuArray + " " + inputText);
            if (skuArray.includes(inputText)) {
                document.getElementById("msg").innerHTML = "* This SKU already exists!";
            }
            else document.getElementById("msg").innerHTML = "";
       //     if  php? (in_array( input text , $uniqueSkuArray))                   
        //        $msg = "* This SKU already exists!";              or foreach check return false (what would it do?) and msg; checking inarray in js?
        //or just pass skuarray (which is initialized beforehand with loading the form) - in separate script initializing file; SkuValues class
        }

//instead of script - depending on product type - include necessary .php/html file - no problem with unnecessarily filled data and +/-required inputs.
    </script>

<body>
        
    <header>
        <h1>Product Add</h1>    
        
        <!--header buttons for Product Add page -->
        <div class="btn-div">
            <!--    <button type="submit" form="product_form" id="save-product-btn" class="btn" value="save" >Save</button>  -->
            <input type="submit" form="product_form" name="save_button" id="save-product-btn" class="btn" value="Save" />
            <a href="index.php"> <button type="button" id="cancel-btn" class="btn" value="cancel">Cancel</button></a>
        </div>
    </header>
<!-- maxlength does not work for numbers -->  
<!-- max and min work -->
    <section>
        <div class="product-form">
            <form id="product_form" name="product_form" method="post" action=""> <!-- action="/functions/function_name" onsubmit="return validateFields();" -->  
            <fieldset id="fieldset-main">
                <label for="sku">SKU</label>
                <input id="sku" name="sku" type="text" maxlength="100" required oninput="validateSku()" > <p id="msg"><?php if(isset($msg)) echo $msg; ?></p> <!-- or onblur; and can use js innerHTML text writing instead  -->
                <br><br>
                
                <label for="name">Name</label>
                <input id="name" name="name" type="text" maxlength="100" required><br><br>
                
                <label for="price">Price</label>
                <input id="price" name="price" type="number" step="0.01" min="0" max="99999999.99"required><br><br>  
                <!-- type number only allows to use dot and does not react to comma or letters(unless they are in the previously used list); also has up-down arrows for incrementing -->
                <!-- step to allow decimal values, with two decimals in this case -->
                   
                <label for="productType">Type Switcher:</label>
                <select id="productType" name="productType" onchange="showProductParameters('productType')" required>
                  <option value="" selected>Type Switcher</option>                   
                  <option value="dvd">DVD</option>
                  <option value="book">Book</option>
                  <option value="furniture">Furniture</option>
                </select>
            </fieldset>    
<!-- "option IDs" a.k.a fieldset IDs that are responsible for those options; 
option value === fieldset id, so functions shouldn't be changed if other options&fieldsets ar added based on this version -->                
            <fieldset id="dvd">
                <label for="size">Size (MB)</label>
                <input id="size" name="size" type="number" min="0" max="99999999999" value="" required><br>   
                <p class="product-description">Please, provide size (in MB)! Use whole numbers. </p>                
            </fieldset>
                <!-- !!! before number type, decimals where allowed and rounded automatically in the table; now only whole numbers are allowed (with step="0.1" etc would alllow to write in form, but round up/down upon sending-->
            <fieldset id="book">
                <label for="weight">Weight (KG)</label>
                <input id="weight" name="weight" type="number" min="0" max="99999999999" value="" required><br>
                <p class="product-description">Please, provide weight (in KG)! Use whole numbers. </p>
            </fieldset> 
              
            <fieldset id="furniture">
                <label for="height">Height (CM)</label>
                <input id="height" name="height" type="number" min="0" max="10000" value="" required><br><br> <!-- 100m should be enough for furniture -->
                
                <label for="width">Width (CM)</label>
                <input id="width" name="width" type="number" min="0" max="10000" value="" required><br><br>
                
                <label for="length">Length (CM)</label>
                <input id="length" name="length" type="number" min="0" max="10000" value="" required><br>
                <p class="product-description">Please, provide requested dimensions (using centimeters) for the furniture! <br/>  Use whole numbers. </p>
            </fieldset> 
        </form>
        </div>            
    </section>  
    
    <footer>
        <p><?php echo "Scandiweb Test assignment"  ?></p>
    </footer>       
        
</body>
    

</html>
