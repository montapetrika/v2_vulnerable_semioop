<?php


class Products extends Database {
//    private $tableName = "products"; ???
 //   private function query() {$queryList = "SELECT * FROM " . $this->tableName . " ;}    ???
  //  private $queryList = "SELECT * FROM products ORDER BY product_id ASC" ;
    
    private $queryList = "SELECT * FROM products ORDER BY product_id ASC" ;
    
/*
    public function __construct()
    {
        $this->getProductListArray();
        $this->showProductListAsTable();
        $this->showProductItems();
    }
 */
    
    public function getProductListArray() {                //__construct() {
                                    //$database = new DatabaseConnection(); //if no class extending
                                    // $con = $database->dbConnect();       
                                    // $listArray = mysqli_query($con, $this->queryList);
        $data = $this->dbConnect()->query($this->queryList);   //contains both mysqli part from dbConnect() and query part afterwards.
        $numRows = $data->num_rows;        //$data is somehow not yet an array..., needs to be read into an array
        if($numRows > 0) {
            while ($row = $data->fetch_assoc()) {          //assoc returns column names(assocciative array); fetch_array - both index and column name are usable
                $listArray[] = $row;
            }
        return $listArray;   //an array of data
        }
        //used to be return $data; //before $numRows and $listArray were introduced, then the table in index.php was created by while ($row = mysqli_fetch_array($variable name in that file for this return))
        // fetch_array
    }
    
    protected function getProductId() {
        
    }
    protected function getProductSku() {
        
    }

    
    protected function setProductSku() {
        
    }
    //and so on; and all variable names the get replaced with the $this->method() call
    

      function showProductListAsTable() { 
        $listArray = $this->getProductListArray();
                                       //no cita faila/klases izsaucot:
                                    // $productList = new ProductList();
                                    //$listArray = $productList->getProductListArray();
            $a = '<table>';
            $a .= '<tr><th>product_id</th>';
                $a .= '<th>sku</th>';
                $a .= '<th>name</th>';
                $a .= '<th>price</th>';
                $a .= '<th>product_type</th>';
                $a .= '<th>size</th>';
                $a .= '<th>weight</th>';
                $a .= '<th>height</th>';
                $a .= '<th>width</th>';
                $a .= '<th>length</th></tr>';
                        //used to be...
                //	$query = 'SELECT * FROM products';
                //	$listArray = mysqli_query($con,$query);
                //      while ($row = mysqli_fetch_array($listArray)) //because it seems that php does not look at sql tables as arrays, data needs to be added to an actual array with this built-in method or fetch_assoc(); which now was done in class/method already                
        foreach($listArray as $row)                
        {
            $a .= '<tr><td>'.$row['product_id'].'</td>';
            $a .= '<td>'.$row['sku'].'</td>';
            $a .= '<td>'.$row['name'].'</td>';
            $a .= '<td>'.$row['price'].'</td>';
            $a .= '<td>'.$row['product_type'].'</td>';
            $a .= '<td>'.$row['size'].'</td>';
            $a .= '<td>'.$row['weight'].'</td>';
            $a .= '<td>'.$row['height'].'</td>';
            $a .= '<td>'.$row['width'].'</td>';
            $a .= '<td>'.$row['length'].'</td>';
        }
        $a .= '</table>';
        return $a;
            
    }
    
     function showProductItems() {                        // showProductListAsGrid() { 
        $dollars = " $";
        $MB = " MB";
        $KG = " KG";
        
         $listArray = $this->getProductListArray();
                                       //no cita faila/klases izsaucot:
                                    // $productList = new ProductList();
                                    //$listArray = $productList->getProductListArray();
           $a = '<div class="product-list-container">';          //if I wanted to pass not just items but all block straight away, so it could be called as a whole div block in any file;
           $a .= '<div class="centered-product-list-container">';
           $a .=  '<form id="product_list" name="product_list" method="post" action=""> ';                    // action="'. $_SERVER['PHP_SELF'].'"> ';
                         
                        //used to be...
                //	$query = 'SELECT * FROM products';
                //	$listArray = mysqli_query($con,$query);
                //      while ($row = mysqli_fetch_array($listArray)) //because it seems that php does not look at sql tables as arrays, data needs to be added to an actual array with this built-in method or fetch_assoc(); which now was done in class/method already                
        
        foreach($listArray as $row)                
        { 
            $a .= '<div class="product-item">'; 
            $a .= '<input class="delete-checkbox" name="delete_checkbox[]" type="checkbox" value="'.$row['product_id'].'">';            
            $a .= '<p>'.$row['sku'].'</p>';
            $a .= '<p>'.$row['name'].'</p>';
            $a .= '<p>'.$row['price'].$dollars.'</p>';
            if (!empty($row['size']))
            {$a .= '<p">Size: '.$row['size'].$MB.'</p>';}
            if (!empty($row['weight']))
            {$a .= '<p>Weight: '.$row['weight'].$KG.'</p>';}
            if (!empty($row['height'])) //and the rest, but html validation resolves that
            {$a .= '<p>Dimensions: '.$row['height'].'x'.$row['width'].'x'.$row['length'].'</p>';}
            $a .= '</div>'; 
                //  $productItems[] = $a;       // with return $productItems; at the end; without outer container div and form; print_r works
        }
        $a .= '</form></div></div>';

        return $a;                // if this was return $var, then html part would contain <?php echo $objectInstance->method(); print_r($array_name) works to see, what the array looks like with formatting;
            
    }
}
    

 ?>