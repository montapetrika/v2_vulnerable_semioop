<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of skuvalues
 *
 * @author Monta Petrika
 */
class SkuValues extends Products {
    
/*    public function __construct() {
        return $this->getAllUniqueSku();
        }*/
// consturct does not return any array values...
    
 // every column name etc could be defined as a variable in some class/config file (abstract, not hard-coded    
    public function getAllUniqueSku() {  //dublicates can still be uploaded straight into DB with SQL through phpAdmin, though not through the form //to do - not show sku dublicates? (or if everything the same) / warning that there are several of these...
        $productListArray = $this->getProductListArray();
        $uniqueSkuArray = [];
        foreach ($productListArray as $row => $observation) {         //whole table as a row number => whole observation
            foreach ($observation as $colName => $cellValue) {          //each observation as column name => cell value
                if ($colName == "sku") {
                    if (!in_array($cellValue, $uniqueSkuArray))     //DB has UNIQUE constraint, so not really needed here, does not upload dublicates
                        $uniqueSkuArray[] = strtoupper($cellValue);         //js and php sku value comparison is CASE SENSITIVE - for sku validation (otherwise possible to get error from DB (UNIQUE still catches it as last resort)
//if database contains anything other than same case letters                
                    
                }
            }           
        }
        return $uniqueSkuArray;
    }
    
    public function jsonSku() {
        return json_encode($this->getAllUniqueSku());
    }
}
