<?php 
$filepath = realpath(dirname(__FILE__));
include_once($filepath.'/../config/config.php');
?>

<?php
class Database {

/**    
*    private $hostname = 'localhost';
*    private $user = 'root';
*    private $password = '';
*    private $dbName = "juniortest";	
**/

    private $hostname    = DB_HOST;
    private $user        = DB_USER;
    private $password    = DB_PASS;
    private $dbName      = DB_NAME;
    private $port        = DB_PORT;
 //   protected $table1    = DB_TABLE1;
/*    
    public function __construct()
    {
        $this->dbConnect();
    }
 */   
    
    protected function dbConnect () {
// No connection could be made because the target machine actively refused it. (without the port)
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        $con= new mysqli(
                $this->hostname, 
                $this->user, 
                $this->password, 
                $this->dbName, 
                $this->port);
//        mysqli_set_charset($con,"utf8");
        if(!$con) {
            die("Connection failed: ". mysqli_connect_error()); 
//'Connection error: ' . $con->connect_error;
        } 
        return $con;
    }
    
}


?>
    

