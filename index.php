<?php
require 'scripts.php';  //could be header, when I learn how to automatically change title text and buttons
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
	<meta name='author' content='Monta Petrika' />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Junior Developer Test Task for Scandiweb</title>
        <link rel="stylesheet" type="text/css" href="style.css" />              
    </head>  
<body>
        
    <header>
        <h1>Product List</h1>         
        <!-- header buttons for Product List page -->
        <div class="btn-div">      
            
<!-- function for creating PRODUCT LIST (a.k.a FORM for checkbox purposes) is in PRODUCTS.PHP page -->  
<!-- formaction = "", so no redirecting would take place, and because scripts.php has the needed function in 'plain view' -->

           <a href="add-product.php"> <button type="button" id="lead-to-form-page-btn" class="btn" value="add">ADD</button></a> 
           <input type="submit" form="product_list" name="delete_button" id="delete-product-btn" class="btn" value="MASS DELETE" />
       <!--      <button type="submit" name="delete_button" form="product_list" id="delete-product-btn" class="btn" action="functions.php" value="delete">MASS DELETE</button>   --> 
        </div>
    </header>
    
    <section>
        <?php
        //    $database = new DatabaseConnection();
        //    $con = $database->dbConnect();       
        ?>
        <div class="product-form">
            <?php
            $viewProductList = new Products();
            echo $viewProductList->showProductListAsTable();
                                                    //$productListTable = $viewProductList->showProductListAsTable(); echo $productListTable;
             ?>
       </div>
    <!--    <div class="product-list-container">
            <form id="product_list" name="product_list" method="post" action="<?php // echo $_SERVER['PHP_SELF']; ?>"> -->
                <?php
            
           echo $viewProductList->showProductItems();
            
         //   $testObj = new Test(); $testObj->getFormDataStmt("abc");
         /*   $test = new Updating();
            $test->deleteProduct();*/ 
        
        //to see how the table looks as an array -> sku values are 2 levels down (2 foreach)   
           $products = new Products();
           $productsArray = $products->getProductListArray();
           print_r($productsArray);
           ?><br/><?php
        /*   $uniqueSkuArray = $products->getAllUniqueSku();
           print_r($uniqueSkuArray);*/
           $skuObject = new SkuValues();
           $uniqueSkuArray = $skuObject->getAllUniqueSku();
           print_r($uniqueSkuArray);
            ?>
        <!--    </form></div> -->
<br/><br/>
<?php

echo json_encode($uniqueSkuArray); // ["apple","orange","banana","strawberry"]
?>
<br/><br/>
<?php
echo $skuObject->jsonSku(); // 
?>
              
        
        
        
        
    </section>  
    
    <footer>
        <p><?php echo "Scandiweb Test assignment"  ?></p>
    </footer>       
        
</body>
    

</html>
