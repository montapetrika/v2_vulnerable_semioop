<?php


//    in main documents:
// require 'includes/class-autoload.php';

/*
spl_autoload_register(function ($class) {
    include_once "classes/".$class.".php";
});
*/


spl_autoload_register('myAutoLoader');

function myAutoLoader($className) {
    $path = "classes/";
    $extension = ".php";
    $fullPath = $path . $className . $extension;   
    
    if(!file_exists($fullPath)) {
        return false;
    }
    
    include_once $fullPath;
}
    
    
    
    
?>

